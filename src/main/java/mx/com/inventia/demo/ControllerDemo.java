package mx.com.inventia.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demoInventia")
public class ControllerDemo {

	@Autowired
	private ServiceDemo serviceDemo;
	
	@GetMapping("/inicio")
	public String demoInicio() {
		return "{status:ok}";
	}
	
	
	@GetMapping("/varios")
	public String varios() {
		return serviceDemo.serviceDemo();
	}
	
	
	@GetMapping("/error")
	public String error() {
		return serviceDemo.error();
	}
	
	public ServiceDemo getServiceDemo() {
		return serviceDemo;
	}


	public void setServiceDemo(ServiceDemo serviceDemo) {
		this.serviceDemo = serviceDemo;
	}
}
